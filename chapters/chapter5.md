---
title: 'Chapter 5: Introduction To International Political Economy (IPE)'
description:
  'This course provides a comprehensive understanding of the complexities of international trade and business. Through a historical lens and contemporary analysis, you will gain valuable insights into the forces shaping the global economy and acquire strategic skills for navigating the international business landscape.'
prev: /chapter4
next: /chapter6
type: chapter
id: 5
---

<exercise id="1" title="Introduction To International Trade And Economy"> 

## Introduction to International Political Economy 

<br>
<center> <iframe width="560" height="315" src="https://www.youtube.com/embed/cJRQkwMyup8?si=fwpdwdIHz-nGjm0Z" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> </center>
<br>

International Political Economy (IPE) is a multidisciplinary field that explores the complex interactions between political and economic forces globally. IPE examines topics such as international money, finance, trade, investment, and the environment. As a field, it spans across academic disciplines including political science, economics, sociology, and history. IPE seeks to understand the power relations that condition economic transactions at the domestic, international, and global levels.

Some key topics examined in IPE include:

- International trade and trade policy 
- International finance and monetary systems
- Development economics and foreign aid
- Globalization and regional integration  
- Role of international institutions like the IMF and World Bank
- Multinational corporations and foreign direct investment
- International migration and labor markets
- Financial crises and economic instability
- Environmental issues and sustainability

IPE explores how political forces shape economic interactions and vice versa. It looks at how states, intergovernmental organizations, non-state actors like NGOs and multinational corporations, as well as individuals and social groups use power to influence transnational economic relationships. The field equips us with the analytical tools to understand the political underpinnings of economic outcomes in an increasingly interconnected world.

## Importance of Understanding IPE

International Political Economy (IPE) is important because it serves as an analytical tool for comprehending the intricate triangle relations among states, markets, and societies on a global scale. The scope of IPE spans across several interconnected domains:

- **Security structures** - IPE examines how global financial crises can impact state military funding and conditions surrounding war, terrorism, and emerging power dynamics.

- **Production and trade** - IPE investigates how crises affect foreign investment, international trade flows, and currency values. 

- **Finance and monetary matters** - IPE looks at institutions' ability to respond to economic shocks and how new political players can influence domestic and international coalitions and policies.

- **Knowledge and technology** - IPE explores how economic upheaval shapes consumption trends, inequality, and access to services like education and healthcare.

By studying the complex interplay between political and economic forces in these areas, IPE provides crucial insight into the rapidly integrating world economy and globalized society we live in. The analytical perspective it offers helps reveal the hidden connections tying our lives to global systems.

## IPE and International Relations

In the realm of International Relations, IPE investigates the complex interactions between political and economic forces that shape relations between states. Some key issues examined include:

- How global financial crises affect state military funding and the conditions of war or terrorism. For example, an economic recession may force states to cut military budgets, limiting their ability to engage in conflicts abroad. However, high unemployment and domestic unrest resulting from a crisis could also increase the risk of political violence.

- Whether rising economic powers such as China and India can translate their growing economic strength into greater political influence in global affairs. Countries that were once mainly economic players are seeking more significant roles in international institutions like the United Nations, G20, and World Trade Organization. However, established powers may be reluctant to cede influence.

- The degree of interdependence between countries based on cross-border economic ties. Higher trade and financial integration may promote peace by raising the costs of conflict. But economic interdependence can also enable the spread of economic shocks. For instance, the 2008 global financial crisis that began in the U.S. impacted countries worldwide.

- How economic sanctions and incentives are utilized as tools of statecraft. Imposing sanctions on a country can leverage economic pain to achieve political ends but can also have indiscriminate effects on innocent populations. Alternatively, preferential trading access and other economic carrots can be offered to reward certain behavior.

In essence, IPE explores how international economic activity enables and constrains the foreign policy options of states in achieving national interests. Economic relations and motivations are deeply intertwined with political interactions between countries in the international system.

## IPE and Comparative Politics 

In the realm of Comparative Politics, International Political Economy delves into political institutions' ability to respond to unemployment and the effects of emerging political forces on coalitions. 

Specifically, IPE analyzes how economic crises and shifts impact domestic political structures and test their resiliency. For example, periods of high unemployment tend to increase social unrest and anti-establishment political movements. IPE examines how existing political institutions adapt policies and reforms in response, such as expanding unemployment benefits or job retraining programs. However, institutional inertia and path dependency can make implementing timely and adequate responses difficult.

IPE also looks at how new political parties and movements resulting from economic disruptions can reshape domestic political coalitions. If these emerging forces gain traction, they may be able to enact major reforms by building novel electoral coalitions. However, existing elites often resist such challenges to their power. The interplay between entrenched interests and rising populism sparked by economic changes is a key focus within comparative politics.

Understanding these complex dynamics provides insight into how global economic forces interact with and transform domestic politics. IPE equips comparative political scientists with the tools to analyze institutional resilience, coalition shifts, and the balance of power between establishment groups and counter-movements. Grasping these relationships is crucial for comprehending the political fallout from globalization and economic integration.

## IPE and Sociology

From a Sociological perspective, IPE explores how economic and political dynamics affect society. This includes investigating how major events like financial crises influence consumption trends and shape inequality.

Specifically, sociologists examine how economic shocks lead to changes in consumer behavior. For example, a recession may force households to cut back on discretionary purchases, switch to cheaper brands, or postpone major expenditures. These shifting consumption patterns reveal which segments of society bear the brunt of the downturn. 

Sociologists also analyze how crises exacerbate existing inequalities or create new ones along ethnic, gender, and other demographic lines. Periods of economic turmoil often disproportionately affect minorities and other marginalized groups who have fewer resources to withstand the shocks. The effects can range from reduced access to education and health care to increased housing insecurity and unemployment. 

Gender inequality also tends to rise during crises as women are more likely to be laid off or experience reductions in wages and benefits. Financial necessity may even compel more women to enter the informal economy in precarious jobs. Overall, IPE provides sociologists with critical insights into how major economic events affect social stratification and equity. Analyzing these dynamics is key to understanding the societal impacts of globalization.

## Politics in IPE

Politics involves creating rules for states and societies to achieve their goals, both in public and private institutions with the authority to pursue different objectives. 

Governments create laws and regulations to provide structure for economic and social activity. These rules allow nations to direct activity towards desired national objectives. For example, countries may incentivize domestic manufacturing to promote economic growth and employment. Alternatively, they could create foreign investment requirements to gain access to capital and technology. 

Within societies, private institutions like corporations and civic organizations also create rules to meet their goals. Corporations establish guidelines for operations, production, marketing and other business functions. Unions create collective bargaining agreements outlining wages and benefits. Advocacy groups promote policies for social causes through activism and lobbying.

IPE analyzes how the political interactions between states, private institutions, and social movements shape economic systems locally and globally. It examines how these groups use their authority and influence to pursue sometimes complementary and sometimes competing aims. Their relative power and cooperation or conflict fundamentally impacts international flows of money, trade, resources and information.

Understanding these complex dynamics provides insight into how political forces interact with economics at all levels. This knowledge helps explain economic outcomes and identify means to address economic problems through both governmental and private action. Political priorities and ideologies fundamentally drive economic systems and outcomes. IPE provides a crucial framework for analyzing how.

## Perspectives on IPE

IPE is further analyzed through three dominant perspectives: 

### Economic Liberalism (neo-liberalism)

Economic liberalism, also known as neo-liberalism, advocates for laissez-faire policies and free market capitalism with little to no government intervention. According to economic liberals, the market is efficient at allocating resources and government intervention tends to create inefficiencies. They argue that barriers to trade, such as tariffs and regulations, negatively impact economic growth and should be reduced. Economic liberals support free trade, privatization, and deregulation.

### Mercantilism (economic nationalism) 

Mercantilism, also referred to as economic nationalism, focuses on maximizing exports, minimizing imports, and accumulating wealth by running trade surpluses. According to mercantilists, the global economy is zero-sum, and one nation's gain is another's loss. They promote protectionist trade policies like tariffs and quotas that restrict imports, subsidize domestic industries, and regulate foreign corporations. The goal is to make the nation self-sufficient, grow the economy, and increase state power.

### Structuralism

Structuralism examines how political, social, and economic structures shape interactions between developed core countries and less-developed periphery countries. It views the global capitalist system as inherently exploitative and unequal, systematically favoring wealthy countries over poorer ones. Structuralists claim rich countries perpetuate their dominance through political influence in global institutions, multinational corporations, and control of capital flows and technology. They criticize economic liberalization for magnifying global inequality and underdevelopment. Structuralists advocate reforms like debt relief, fair trade, and technology transfers to periphery countries.

## Key Issue Areas in IPE

Scholars simplify the study of the global economy by dividing it into key issue areas such as international trade, international monetary systems, multinational corporations, and economic development. These areas are deeply interconnected, and scholars examine how the political dynamics between winners and losers in the global economy shape these systems and their consequences.

### International Trade

International trade refers to the exchange of goods and services between countries. Key issues examined under international trade include trade policies, trade patterns and flows, and the international trade system. Scholars analyze how political factors influence trade policies and regulations and the winners and losers under different trade regimes.

### International Monetary Systems 

The international monetary system facilitates cross-border payments and transactions between countries. It consists of institutions, rules, and procedures that govern currency exchange rates and cross-border capital flows. Scholars examine how political interests shape international monetary policies and regulations and how these impact different countries and groups.

### Multinational Corporations

Multinational corporations operate business activities in multiple countries. Their complex operations intersect with politics as they influence regulations, taxes, trade, and investment policies. Scholars analyze multinationals' political power, how they impact host countries, and the regulatory challenges they create.

### Economic Development 

Economic development refers to improving economic well-being, often focusing on poorer countries working to improve standards of living. Scholars examine how political priorities and governance capacities shape development policies and outcomes. They also analyze how economic development programs impact domestic politics and power dynamics.

The interconnectivity across these issue areas creates complex dynamics that IPE scholars work to unravel. For example, trade policies impact currency values, capital flows affect corporations' offshore operations, and development funding relies on global economic institutions. IPE provides an integrative lens to examine these multilayered economic and political relationships.

## International Economics

International Economics focuses on how nations interact through trade, money flows, and investment. Micro-economics explores the benefits of trade, trade patterns, and government trade policy. Macro-economics delves into the balance of payments, exchange rate determination, international policy coordination, and the international capital market. International Economics includes both real transactions, such as international trade, and financial transactions, such as international money.

**International Trade**

International trade involves the exchange of products and services across borders. Countries trade with each other to obtain goods and services that they cannot produce themselves or can produce more efficiently. The benefits of international trade include greater efficiency, economies of scale, increased production, more consumer choices, and market competition. Trade allows countries to specialize in products they can produce most efficiently and import products that other countries can produce more efficiently. However, international trade can also lead to job losses in import-competing sectors. Governments influence trade through tariffs, quotas, subsidies, and trade agreements. Microeconomics analyzes the patterns of trade and the impact of government trade policies.

**International Money Flows** 

International money flows refer to the exchange of currencies between countries for purposes of trade, investment, speculation, and policy. Money flows enable international transactions for goods, services, and assets. The balance of payments tracks money flowing in and out of a country from international trade, financial transactions, and unilateral transfers. Macroeconomics examines exchange rate determination, which impacts the prices of imports and exports. Macroeconomics also studies how central banks and governments manage international money flows through monetary policy and currency interventions. For example, quantitative easing and higher interest rates affect the value of a currency. Coordinated monetary policy between central banks can promote economic stability.

**International Investment**

International investment involves the transfer of assets or ownership across borders. Foreign direct investment (FDI) includes establishing foreign enterprises or acquiring foreign companies. Portfolio investment refers to purchasing minority stakes in foreign financial assets and stocks. Governments aim to attract FDI to create jobs, generate tax revenue, and facilitate technology transfer. However, some are concerned about foreign control and capital flight. Macroeconomics analyzes how the international capital market impacts interest rates, credit flows, and exchange rates between countries. The free flow of capital can exacerbate economic crises but can also provide valuable financing for developing nations.

## International Business

International business serves as both a cause and a result of increasing national prosperity, providing a competitive advantage for firms. Expanded global operations allow companies to increase sales, maximize returns, achieve global scale economies, and acquire resources not available domestically. However, international business also has societal implications that must be considered.

Multinational corporations expanding abroad can contribute to job creation, currency circulation, and general economic development in host countries. This leads to rising prosperity, increasing literacy, improved nutrition, and greater access to healthcare. However, some critics argue that multinationals do not invest enough in developing local economies and communities. Questions remain around corporate social responsibility and environmental stewardship.

Globalization enabled by international business provides competitive advantages for companies in sales, profits, scale, and resources. But it also exposes domestic industries and employees to disruptive global competition. Economies can benefit from new innovations and efficiencies. But traditional sectors face pressures to innovate rapidly or lose market share to foreign rivals. Policymakers must balance these complex factors when regulating international business.

For individuals, working for multinationals or starting international ventures provides diverse experiences, expands perspectives, and creates unique career opportunities. The confidence gained from bridging different cultures helps develop cosmopolitan worldviews. But connecting people globally also enables rapid spread of harmful or unethical business practices across borders. Overall, international business integrates the world economically but raises important societal questions around shared responsibilities.

</exercise>

<exercise id="2" title="Approaches to IPE: Mercantilism vs. Liberalism"> 


## Introduction to Mercantilism and Liberalism

Mercantilism and liberalism represent two major, and at times competing, schools of thought in international political economy. Mercantilism emerged in the seventeenth and eighteenth centuries, developing theories linking economic activity and state power. Core mercantilist ideas emphasize the interconnection between national power and economic strength, see trade as a means for states to acquire wealth, and prioritize certain economic sectors deemed most valuable. 

In contrast, liberalism arose in the eighteenth century as an intellectual challenge to mercantilism. Led by thinkers like Adam Smith and David Ricardo, liberalism distinguishes between economics and politics, argues that trade benefits countries regardless of surpluses or deficits, and contends that wealth derives from efficiently producing goods and engaging in trade, not just manufacturing. Liberalism advocates market-based allocation of resources to maximize individual welfare.

While differing substantially, mercantilism and liberalism have both significantly shaped modern perspectives on the relationship between states, markets, and economic policy. Their contrasting theories continue to inform debates around international political economy.

## Core Principles of Mercantilism

Mercantilism is rooted in theories from the 17th and 18th centuries that link economic activity and state power. At its core, mercantilism holds three central propositions:

- National power and wealth are intricately connected. Mercantilists argue that a country's economic and military power are directly tied together. They believe that a strong economy is necessary to support military strength and national security.

- Trade is a means for countries to acquire wealth from abroad. Mercantilists view trade as a zero-sum game where countries compete to amass wealth at the expense of their trading partners. The goal is to maximize exports while minimizing imports to achieve a trade surplus and accumulate gold reserves.

- Certain economic activities hold more value. Mercantilists place greater importance on economic activities like manufacturing, mining, fisheries, and agriculture since they produce tangible goods for trade. Less value is placed on services or retail since they don't directly contribute to exports.

The mercantilist argument asserts that national power and wealth are inextricably linked. Economic activities that strengthen exports are prioritized as key to state power in this worldview.

## Mercantilist Trade Policy

One of the key principles of mercantilism was that trade should be used as a means for a country to acquire wealth from abroad. This led to policies that favored maximizing exports while minimizing imports. 

Mercantilists believed that a country could amass wealth by encouraging production of goods at home, then exporting those goods to other countries. The funds from selling exports would result in an inflow of treasure and wealth. At the same time, mercantilist policies aimed to discourage importing goods from other countries, as this was seen as a drain of a nation's wealth to foreign lands. 

Some common mercantilist trade policies included:

- High tariffs on imported manufactured goods to discourage imports and protect domestic industries. This allowed local producers to charge higher prices.

- Low or no tariffs on imported raw materials to support local manufacturing.

- Subsidies, state aid, and protection for industries considered important for a nation's economic and military power. This included manufacturing, shipping, and high-tech industries of the time.

- Promoting the colonies as a captive market for exports from the home country. The colonies were also a source of cheap raw materials.

- Monopoly trading companies and trade routes to control trade and amass maximum benefit.

Through this intentional steering of trade flows to create net export surpluses, mercantilists believed a nation could continually build its stock of treasure and wealth at the expense of its trading partners. This view of trade as a zero-sum game, where one side benefits at the other's expense, was core to mercantilist trade theories. It stood in sharp contrast to the liberal perspective that would emerge later.

## Mercantilist Economic Priorities

Mercantilism places significant value on certain economic activities over others, specifically high-technology manufacturing. In the mercantilist view, manufacturing activities hold inherent economic value, driving innovation and providing skilled employment. Manufactured goods were prioritized for export while raw materials were deemphasized as mere inputs into production. 

The mercantilist emphasis on manufacturing stemmed from its central role in wealth generation during the emergence of capitalism and early industrialization. New manufacturing technologies and production techniques allowed for increased efficiency, scale, and productivity. Mercantilists linked manufacturing prowess to national power, believing states with the most advanced and innovative manufacturing sectors held economic and geopolitical advantages.

With manufacturing elevated as a source of economic strength, mercantilists advocated policies promoting domestic manufacturing. These included subsidies, tax incentives, and other measures to stimulate growth of high-value manufacturing industries. Mercantilists justified such policies as enhancing national power and believed market forces alone were insufficient to develop strategic manufacturing capabilities. This manufacturing-focused economic approach remains influential today, with many viewing manufacturing as vital for innovation, jobs, and global competitiveness.

## Role of the State in Mercantilism

The core mercantilist argument asserts a significant role for the state in resource allocation decisions, believing economic activity is too vital to be left to uncoordinated market processes. Mercantilism holds that the state should actively manage economic policies and intervene in the economy to promote national power and wealth. This includes using tariffs, quotas, subsidies, and other measures to encourage exports, restrict imports, and favor certain domestic industries over others. 

Mercantilists contend that the free market alone cannot properly allocate resources in a way that maximizes national power. They argue that economic activities are intricately tied to state power, so the state must coordinate production, trade, and economic development. Without strong state oversight and intervention, mercantilists believe the economy would operate inefficiently and fail to strengthen the nation. Therefore, under mercantilism, the state plays a central, coordinating role in all aspects of the economy to harness economic activity for national objectives. This represents a significant difference from later schools like liberalism that advocate limiting the economic role of the state in favor of free markets.

## Emergence of Liberalism

Liberalism emerged in the 18th century as a challenge to mercantilism, led by prominent British thinkers such as Adam Smith and David Ricardo. 

Smith, often considered the father of modern economics, published his seminal work *The Wealth of Nations* in 1776. This text formed a foundation for classical liberal economics by arguing that nations prosper not from stockpiling gold and erecting trade barriers, but by allowing individuals the freedom to pursue their own self-interest within a competitive market system. 

Smith contended that trade imbalances are self-correcting and mutually beneficial for countries. His theory of absolute advantage demonstrated how countries can benefit by specializing in goods they can produce most efficiently and trading with partners, rather than trying to be self-sufficient. Smith also diverged from mercantilism by claiming that a nation's wealth lies not just in physical goods like gold and silver, but in the productive capabilities and labor of its citizens.

Building on Smith's ideas, David Ricardo formalized the theory of comparative advantage in his 1817 book *On the Principles of Political Economy and Taxation*. Ricardo argued that even if a country is more efficient at producing every good than its trading partners, it still benefits from specializing in products where it has the greatest advantage and importing other goods. This novel perspective further shifted economics away from mercantilist zero-sum attitudes about trade.

Together, Smith and Ricardo systematically dismantled major tenets of mercantilism and developed foundational concepts that still underpin free market economics today. Their work spearheaded the transition from policies aimed at amassing bullion and running trade surpluses to a new liberal focus on deregulation, efficiency, and mutual gains from trade.

## Liberalism Distinguished Economics and Politics

Emerging in eighteenth-century Britain, liberalism made a key distinction between economics and politics that differentiated it from mercantilism. Adam Smith, David Ricardo, and other early liberal thinkers argued that economic activities should aim to enrich individuals, not enhance state power. 

Liberals contended that individuals should be free to make economic decisions in their own self-interest through voluntary transactions in markets. Rather than the state controlling economic matters, liberals believed that the invisible hand of market forces would allocate resources efficiently. They saw the role of the state as creating the conditions for open and fair competition, not intervening to achieve preferred economic outcomes.

In the liberal view, individuals undertaking economic activities to improve their own welfare would collectively produce broader social benefits. This stood in contrast to mercantilists who saw economic policy primarily as a tool to strengthen the nation-state. By decoupling economics from state power, early liberals developed theories that provided the intellectual foundation for free market capitalism.

## Liberalism on Trade

Liberalism challenged the prevailing mercantilist belief that countries only benefit from trade when maintaining a surplus in the trade balance. The liberal economic thinkers of the 18th century argued that both the exporting and importing country benefit from trade, regardless of which country has a trade surplus or deficit. 

According to liberalism, the purpose of trade is not accumulating wealth for the state, but rather increasing the welfare and consumption choices of a country's citizens. As such, liberal thinkers contended that countries are better off trading goods that they can efficiently produce at home in exchange for goods that are expensive to produce domestically. This allows all trading partners to enjoy access to a wider variety of goods than they could produce efficiently on their own.

Liberalism holds that voluntary trade is mutually beneficial, as long as it is based on comparative advantage rather than achieving surpluses. Since different countries have varying resources and capabilities, they can maximize wealth creation by specializing in goods they can produce most efficiently and trading for whatever else they require. This interdependence can make all trading partners better off. As the economist David Ricardo demonstrated, two countries can gain from trade even if one country has an absolute advantage in producing all goods.

By shifting the focus from trade surpluses to mutual gains, liberalism laid a strong intellectual foundation for the expansion of free trade. It established trade as a positive-sum game with widespread benefits, challenging the view that one country's gain necessarily comes from another country's loss. This liberal trade theory became highly influential in subsequent centuries.

## Liberalism on Wealth Creation

Liberalism challenged the mercantilist belief that national wealth derived primarily from manufacturing and trade surpluses. Instead, classical liberal theorists like Adam Smith argued that a nation's wealth stemmed from efficient production and mutually beneficial trade. 

According to liberalism, countries prosper not by hoarding gold and silver, maintaining trade surpluses, or focusing narrowly on manufacturing. Rather, nations grow wealthier by developing their productive capacities across all sectors of the economy. Using scarce resources efficiently to meet market demands allows countries to specialize based on their competitive advantage.

Liberalism contends that through open markets and international trade, countries can benefit by exporting goods produced efficiently at home and importing products made more efficiently abroad. The resultant improvements in living standards, productivity, and innovative capacities, fuel broader economic growth and development. Thus, liberalism links national wealth, not to specific activities like manufacturing, but to the productivity and efficiency gains delivered by market allocation and international trade.

## Liberalism Resource Allocation

Liberalism advances a market-based system for resource allocation, asserting that the highest societal welfare emerges when individuals freely make decisions about resource use through voluntary transactions in the marketplace. This contrasts with the mercantilist view that economic activities are too important to be left to unregulated market forces. 

According to liberalism, the market efficiently coordinates decentralized information and allows individuals to freely pursue their self-interest. This builds individual and aggregate welfare. Liberal thinkers like Adam Smith contended that an "invisible hand" guides market participants, as if through an invisible hand, to promote ends that benefit society, even if that was not their original intent.

Liberalism prioritizes individual freedom and welfare in economic decision making rather than subordinating individual interests to state power and wealth accumulation. The liberal perspective holds that unconstrained market transactions create greater prosperity. Individuals allocating resources according to personal utility maximizes social welfare since participants directly feel the benefits and consequences of their economic choices.

# Conclusion

|                                                 |                                                      **Mercantilism**                                                     |                                                                                **Liberalism**                                                                                |
|-------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| **Most Important Actor**                        | State                                                                                                                     | Individuals                                                                                                                                                                  |
| **Role of the State**                           | Intervene in the economy to allocate resources                                                                            | Establish and enforce property rights to facilitate  market-based exchange                                                                                                   |
| **Image of the  International Economic System** | **Conflictual**: Countries compete for desirable industries and engage in trade conflicts as a result of this competition | **Harmonious**: The international economy offers benefit to all countries. The challenge is to  create a political framework that enable countries to realize these benefits |
| **Proper Objective of Economic Policy**         | Enhance power of the nation-state in international system                                                                 | Enhance aggregate social welfare                                                                                                                                             |
Source: Oatley, T. H. (2012). _International political economy_ (5th ed). Longman. 


</exercise>

<exercise id="3" title="Approaches to IPE: Constructivism vs. Critical Theory"> 

# Introduction

International Political Economy (IPE) has seen the rise of two influential perspectives - constructivism and Marxism. These perspectives have challenged conventional narratives and assumptions within the field. 

Constructivism contends that ideas, values, norms, and identities play a critical role in shaping actors' behaviors and influencing international political and economic dynamics. It emphasizes the socially constructed nature of factors like interests and identities. Constructivism has become a prominent counterpoint to materialist theories like Mercantilism and Liberalism in explaining motivations behind choices.

In contrast, Marxism originated from Karl Marx's critique of capitalism and his analysis of its inherent contradictions. Marxism focuses on the exploitative nature of capitalist relations of production. It predicts that the internal tensions of capitalism will lead to its eventual collapse and replacement by socialism and communism. Marxism diverges from traditional IPE theories in its argument that corporations, not states or markets, determine resource allocation under capitalism.

## Constructivism Basics 

Constructivism centers on the notion that many key aspects of political and economic relations are social constructs rather than inherent facts or realities. According to constructivism, predominant ideas, values, norms, and identities exert significant influence over the behaviors of states, organizations, and individuals in the international arena. 

At the core of constructivism is an emphasis on the socially constructed nature of central concepts within international relations and international political economy. Ideas, values, norms, and identities do not exist independently in some objective sense. Rather, they come into being through complex processes of social interaction and interpretation. Their meaning and implications emerge gradually through discourse, debate, and shared understanding among social actors.

Accordingly, constructivism views critical aspects of global politics and economics, like power, security, money, or sovereignty, as social constructs. Their tangible meaning and effects stem from collectively held systems of knowledge and modes of comprehension that develop over time. Actors like states and corporations operate based on these intersubjective social structures rather than reacting directly to objective material realities. 

In essence, constructivism grants causal power to immaterial forces like ideas and norms alongside material capabilities. It breaks from rationalist theories that focus predominantly on material factors by asserting that interests and identities are shaped by collectively constructed values and beliefs. This provides an alternative basis for comprehending and explaining political and economic behaviors and outcomes in the international system.

## Core Assumptions 

Constructivism holds four fundamental assumptions about the role of ideas and social constructs in international relations. **First, ideas, not just material capabilities, shape actors' identities and interests**. Constructivists contend that an individual or state's preferences and motivations are not predetermined or fixed. Instead, ideas, values, and social context work together to produce certain identities and desired outcomes. 

**Second, the distribution of ideas in society matters more than material structure**. Constructivists emphasize that ideas operate as forces that impact how political actors define situations and make calculations. The prevalence of particular ideas, norms, or values within a society influences which options actors view as viable.

**Third, agents (individuals, states) and structures (norms, institutions) constitute one another**. Identities and interests are produced through interaction between agents and the social structures in which they exist. Neither has inherent meaning on their own. This mutual constitution continually evolves and changes through practice and negotiation.

**Fourth, anarchy and the international system are social constructs, not objective realities**. Anarchy does not predetermine state interests and behaviors. Rather, it is given meaning through accepted practices and understandings which can transform over time. The international system gains significance only through constructed ideas about appropriate roles and conduct for states.

In summary, constructivism grants ideas and social context causal power equal to material capabilities. It focuses on how collectively held or intersubjective beliefs shape interests and guide choices in international political economy. Constructivists assert that taking ideas seriously is imperative to fully comprehend global relations.

## Influence on International Political Economy

Constructivism has exerted significant influence on the field of International Political Economy (IPE), challenging conventional materialist perspectives. It argues that ideas, values, norms, and identities play a decisive role in shaping economic and political outcomes between states. 

Specifically, constructivism emphasizes the role of ideas in international cooperation and conflict. It contends that when states share common ideas and values, they are more likely to cooperate even without obvious material incentives to do so. In contrast, when states hold competing ideas and norms, cooperation becomes more challenging, and conflict more likely, even between states with economic interdependence. 

Furthermore, constructivism highlights the role of changing ideas and identities in spurring institutional and systemic change in IPE. As intersubjective beliefs evolve within and between states, new possibilities for cooperation can emerge, norms can spread, and innovative institutions can be created. For instance, the rise of environmentalism as a globally shared value enabled unprecedented international cooperation on issues like climate change.

Ultimately, by assigning causal power to ideational factors, constructivism provides a lens to understand how ideas influence international political economy dynamics involving cooperation, conflict, and change in ways that materialist theories cannot fully explain.

## Marxism Origins 

Marxism originated in Karl Marx's critique of capitalism in the mid-19th century. Marx identified two defining features of capitalism: private ownership of the means of production and wage labor. The means of production refers to the facilities, resources, and systems used to produce goods and services, like factories, machinery, and raw materials. Under capitalism, these means of production are privately owned by capitalists (business owners), rather than being publicly owned. 

Wage labor refers to the system where workers sell their labor power to capitalists in exchange for wages. Workers do not own the products they create or the profits generated. The wages they receive represent only a portion of the value created by their labor. 

Marx argued that the value of goods under capitalism derives from the amount of labor invested in producing them. However, capitalists are able to exploit labor by paying only subsistence wages and keeping the remaining surplus value as profits. This extraction of surplus value enables the capitalist class to accumulate vast wealth and capital.

## Capitalism Features

According to Marx, capitalism is defined by two key features: private ownership of the means of production and wage labor. 

Private ownership refers to the means of production - things like factories, machinery, and raw materials - being owned and controlled by private individuals and corporations rather than by the state. This gives the owners power over the whole production process. They make the decisions about what to produce, how to produce it, where to produce it, and what to do with the profits.

Wage labor is the system where workers do not own the means of production and so have no choice but to sell their labor power to those who do own it. Workers exchange their labor time for a wage to survive. The wages paid to workers represent only a portion of the value they create through their labor. The surplus value is kept by the capitalist as profit.

Marx argued that these two pillars of capitalism created an exploitative economic system. The owners of capital profited from paying workers less than the full value their labor generated. This extraction of surplus value was the source of capitalists' wealth accumulation. At the same time, workers were left struggling to survive on subsistence wages.

## Marxist Analysis 

Marx's analysis of capitalism centered on the concept of exploitation, which he believed was inherent in the capitalist mode of production and exchange. According to Marx, capitalists extract surplus value from workers by paying them wages that represent only a fraction of the value they produce through their labor. 

Marx identified the source of profits under capitalism being the surplus value produced by workers and appropriated by the capitalists. He differentiated between labor power, which workers sell to capitalists in exchange for wages, and labor, which is the actual work done during the production process. The value of labor power is equivalent to the subsistence goods necessary to reproduce the worker’s capacity to work. However, workers must labor for longer than is necessary to produce the value of their subsistence wages. The additional value created constitutes surplus value, which the capitalist retains as profit.

Marx further developed the concept of the rate of exploitation, which quantifies the relationship between surplus value and the value of labor power. The higher the rate of exploitation, the greater the surplus extracted from the workers relative to the wages paid to sustain their labor power. According to Marx, the capitalist drive to maximize profits perpetually pushes the rate of exploitation higher, intensifying the conditions of inequality and hardship for the working class.

## Revolutionary Outcomes 

Marxist predictions revolve around the dynamics of capitalism leading to a revolutionary outcome driven by several key economic factors. 

First, Marx contended that capitalism would result in the concentration of capital, with wealth and means of production concentrating into fewer hands over time. As large corporations came to dominate more industries, small businesses and entrepreneurs would be pushed out, unable to compete. This process of monopolization centralizes economic power.

Second, Marx argued that the rate of profit in capitalist economies would inevitably fall over the long run. As competition forced employers to invest in labor-saving technologies that improved productivity, the ratio of profit relative to investment in production would decline. This creates a lag in the economy, with production growing faster than profits.

Finally, Marx highlighted an inherent imbalance between production capabilities and consumption capacity in capitalism. As capitalists strive to maximize profits, workers' wages are suppressed, limiting their ability to purchase goods. This results in overproduction, spurring recurring crises of overaccumulation and overcapacity compared to demand. 

Together, these dynamics were predicted to fuel a revolutionary uprising, as an increasingly desperate and impoverished proletariat revolts against the wealthy bourgeoisie control of capital and means of production. The inherent contradictions and instability of capitalism would precipitate its own demise, according to Marx's analysis.

## Resource Allocation 

In the realm of resource allocation, Marxists argue that large corporations, not states, play a decisive role in determining how society's resources are used. This stands in stark contrast to Mercantilism's emphasis on the state and Liberalism's focus on the market. According to Marxism, states function as agents of the capitalist class, perpetuating the socioeconomic disparities inherent in the capitalist system.

The Marxist view is that corporations, not governments, are the real decision-makers when it comes to allocating capital and resources in society. States may enact policies and regulations, but their actions ultimately serve the interests of the capitalist elite who control the means of production. The free operation of the market, as espoused by Liberalism, is considered an ideological myth that obscures the dominance of big business.

Marxists contend that corporations funnel resources towards investments that maximize profits, not towards socially beneficial goals. Their enormous economic power allows them to shape government policy in their favor. States end up facilitating and protecting the profit-making activities of corporations, rather than intervening on behalf of common citizens. This entrenches inequalities as the capitalist class accumulates more wealth through corporations extracting surplus value from workers.

In summary, Marxism diverges from mainstream IPE theories by identifying corporations as the prime movers in how capital and resources flow through society. Their unchecked power enables the perpetuation of a system where prosperity accrues to capitalists while depriving workers of an equitable share. Only by transitioning to an economic model not driven by corporate profit can true social justice be achieved from a Marxist perspective.

## Conclusion

Constructivism and Marxism offer contrasting perspectives for understanding dynamics in international political economy. While both challenge conventional materialist theories, their core focuses differ. 

Constructivism emphasizes the role of ideas, values, norms, and identities in shaping actors' interests and behaviors. It contends these socially constructed factors can outweigh materialist considerations like military or economic power. In the IPE context, constructivism argues that shared ideas and values are pivotal forces enabling cooperation and driving change.

In comparison, Marxism spotlights the primacy of economic factors, specifically the dynamics of capitalism, class conflict, and modes of production. Marx's analysis predicts capitalism's inherent contradictions will lead to its collapse, catalyzed by a workers' revolution. Regarding IPE, Marxists stress corporations' central role in allocating resources, rather than states or markets. 

In summary, constructivism grants causal power to immaterial factors like ideas, while Marxism privileges materialist economic forces as the key determinants of societal outcomes. Despite their differences, both theories provide alternative perspectives to conventional IPE frameworks like Mercantilism and Liberalism. Further synthesis of these approaches may yield additional insights into the complex machinations of the global economy.

|                                                 |         **Constuctivism**        |                       **Marxism**                      |
|-------------------------------------------------|:--------------------------------:|:------------------------------------------------------:|
| **Most Important Actor**                        | Individual, group, state         | Classes (particularly the Capitalist)                  |
| **Role of the State**                           | "Facilitator" of norm and rules  | Instrument of the capitalist to sustain capitalism     |
| **Image of the  International Economic System** | Conflict, cooperation            | Exploitative                                           |
| **Proper Objective of Economic Policy**         | Promotion of values, norms, idea | Promote an equitable distribution of wealth and income |
Source: Oatley, T. H. (2012). _International political economy_ (5th ed). Longman. 

</exercise>

<exercise id="4" title="International Trade Regimes"> 


</exercise>

<exercise id="5" title="Mega Trade Deals: TPP and RCEP"> 


</exercise>

<exercise id="6" title="TPP and RCEP: Winners and Losers"> 


</exercise>

<exercise id="7" title=" The Politics of Trade"> 


</exercise>

<exercise id="8" title="Trade and Development"> 


</exercise>

<exercise id="9" title="History and Features of the International Monetary System"> 


</exercise>

<exercise id="10" title="The Politics of International Monetary Regimes"> 


</exercise>

<exercise id="11" title="The Characteristic of Contemporary International Political Economy"> 


</exercise>

<exercise id="12" title="What is Neoliberalism?"> 


</exercise>

<exercise id="13" title="Neoliberalism, Global Governance and Crisis"> 


</exercise>