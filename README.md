# ✨ International Relations Study Guide ✨

This is a free online course on International Relations topics. Use it as your study guide. **It's not intended to become references**. Perfect for self-study. If you want to read more about International Relations topics, I recommend to visit [E-International Relations](https://www.e-ir.info/) website.

[![Netlify Status](https://api.netlify.com/api/v1/badges/66861a3d-1f65-41d1-948a-aea1809f2d83/deploy-status)](https://app.netlify.com/sites/international-relations-studyguide/deploys) 

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/cantikapf/international-relations-study-guide)
---

Build using this [course framework](https://github.com/ines/course-starter-python) made by [Ines](https://github.com/ines) and deployed with [Netlify](https://www.netlify.com/)

---
### Deploy with Netlify

When I first deploy it with Netlify, I struggling to find a right way. If you also face the same problem like me, you can try this instruction below:

**Error code 'Install dependencies': dependency_installation script returned non-zero exit code: 1**

1. Make sure to make ```.nvmrc``` file and write ```10.13.0``` in the file. 
2. If you deploy it in Netlify with Git Hub, change the **Runtime** in **Build Setting** from Gatsby to no selection (you can change it after the first deployment). 

<a href="https://imgbb.com/"><img src="https://i.ibb.co/D1ftH68/Screenshot-2023-12-27-005718.png" alt="Screenshot-2023-12-27-005718" border="0"></a>

**Error code 'Build failed due to a user error: Build script returned non-zero exit code: 2'**

Add Environment Variables with ```SHARP_IGNORE_GLOBAL_LIBVIPS``` in the key section and ```true``` in the values. And finally, ```Generate Public Deploy Key``` in **Continous deployment** menu.

<a href="https://ibb.co/BLCy91d"><img src="https://i.ibb.co/nrL7FWZ/Screenshot-2023-12-27-010037.png" alt="Screenshot-2023-12-27-010037" border="0"></a>

---

### Reference used in this course (update regularly)

**Introuduction to International Relations**

- Baylis, J., Smith, S., & Owens, P. (Eds.). (2017). The globalization of world politics: An introduction to international relations (Second international edition). Oxford University Press.
- Chen, D. (2017, February 27). Is China Ready for Global Leadership? [Online News]. The Diplomat. https://thediplomat.com/2017/02/is-china-ready-for-global-leadership/
- Devetak, R., Burke, A., & George, J. (2012). An introduction to international relations (2nd ed). Cambridge University Press.
- Krauthammer, C. (1990). The Unipolar Moment. Foreign Affairs, 70(1), 23. https://doi.org/10.2307/20044692
- The Economist. (2017, April 1). Is China challenging the United States for global leadership? The Economist. https://www.economist.com/china/2017/04/01/is-china-challenging-the-united-states-for-global-leadership
- Waltz, K. N. (1979). Theory of international politics. Addison-Wesley Pub. Co.

**Introduction to Social Science**

- Hunt, E. F., & Colander, D. C. (2011). Social science: An introduction to the study of society (14th ed). Pearson Education/Allyn & Bacon.
- Little, W., & Little, W. (2014). Chapter 17. Government and Politics. https://opentextbc.ca/introductiontosociology/chapter/chapter17-government-and-politics/


**Modern World History**

- Croxton, D. (1999). The Peace of Westphalia of 1648 and the Origins of Sovereignty. The International History Review, 21(3), 569–591. https://doi.org/10.1080/07075332.1999.9640869
- De Carvalho, B., Leira, H., & Hobson, J. M. (2011). The Big Bangs of IR: The Myths That Your Teachers Still Tell You about 1648 and 1919. Millennium: Journal of International Studies, 39(3), 735–758. https://doi.org/10.1177/0305829811401459
- Dockrill, M. L., Hopkins, M. F., & Dockrill, M. L. (2006). The Cold War, 1945-1991 (2nd ed). Palgrave Macmillian.
Gross, L. (1948). The Peace of Westphalia, 1648–1948. American Journal of International Law, 42(1), 20–41. https://doi.org/10.2307/2193560
- Huntington, S. P. (2011). The clash of civilizations and the remaking of world order (Simon&Schuster hardcover ed). Simon & Schuster.
- Kissinger, H. (2014). World order: Reflections on the character of nations and the course of history. Lane, Penguin.
- Lowe, N. (2013). Mastering modern world history (Fifth edition). Palgrave Macmillan.
- Merriman, J. M. (2004). A history of modern Europe: From the Renaissance to the present (2nd ed). W.W. Norton.
- Mr M Lewis (Director). (2014, December 12). BBC History File: Cuban Missile Crisis. https://www.youtube.com/watch?v=dC4XhIjBPEQ
- Osiander, A. (2001). Sovereignty, International Relations, and the Westphalian Myth. International Organization, 55(2), 251–287. https://doi.org/10.1162/00208180151140577
- Wimmer, A., & Feinstein, Y. (2010). The Rise of the Nation-State across the World, 1816 to 2001. American Sociological Review, 75(5), 764–790. https://doi.org/10.1177/0003122410382639

**Indonesia Political Perspective**

- Aspinall, E., Dettman, S., & Warburton, E. (2011). When Religion Trumps Ethnicity: A Regional Election Case Study from Indonesia. South East Asia Research, 19(1), 27–58. https://doi.org/10.5367/sear.2011.0034
- Aspinall, E., & Sukmajati, M. (Eds.). (2016). Electoral Dynamics in Indonesia: Money Politics, Patronage and Clientelism at the Grassroots. NUS Press. https://doi.org/10.2307/j.ctv1xxzz2
- Ballington, J., Karam, A. M., & International Institute for Democracy and Electoral Assistance (Eds.). (2005). Women in parliament: Beyond numbers (Rev. ed). International IDEA.
- Bessell, S. (2010). Increasing the proportion of women in the national parliament: Opportunities, barriers and challenges. 219–242.
- Budiardjo, M. (2003). Dasar-dasar ilmu politik (Cet. ke-25). Gramedia Pustaka Utama.
- Ethridge, M. E., & Handelman, H. (2015). Politics in a changing world (Seventh edition). Cengage Learning.
- Hillman, B. (2012). Ethnic politics and local political parties in Indonesia. Asian Ethnicity, 13(4), 419–440. https://doi.org/10.1080/14631369.2012.710078
- Hillman, B. (2017). Increasing Women’s Parliamentary Representation in Asia and the Pacific: The Indonesian Experience. Asia & the Pacific Policy Studies, 4(1), 38–49. https://doi.org/10.1002/app5.160
- Mietzner, M. (2006). The Politics of Military Reform in Post-Suharto Indonesia: Elite Conflict, Nationalism, and Institutional Resistance. East-West Center. https://www.jstor.org/stable/resrep06524
- Mietzner, M. (2010). Indonesia’s Direct Elections: Empowering the electorate or Entrenching the New Order Oligarchy? In  null - Edward Aspinall & Greg Fealy (Ed.), Soeharto’s New Order and its Legacy: Essays in honour of Harold Crouch (pp. 173–190). ANU ePress. https://doi.org/10.22459/snol.08.2010.11
- Mietzner, M. (2016). The Sukarno dynasty in Indonesia: Between institutionalisation, ideological continuity and crises of succession. South East Asia Research, 24(3), 355–368.
- Muez, N. (2017). Religion and Politics: Reflections from Jakarta. https://dr.ntu.edu.sg/handle/10356/83323
- Muhtadi, B. (2015). Jokowi’s First Year: A Weak President Caught between Reform and Oligarchic Politics. Bulletin of Indonesian Economic Studies, 51(3), 349–368. https://doi.org/10.1080/00074918.2015.1110684
- Rabasa, A., & Haseman, J. (2002). The Military and Democracy in Indonesia: Challenges, Politics, and Power. RAND Corporation. https://www.rand.org/pubs/monograph_reports/MR1599.html
- Rhoads, E. (2012). Women’s Political Participation in Indonesia: Decentralisation, Money Politics and Collective Memory in Bali. Journal of Current Southeast Asian Affairs, 31(2), 35–56. https://doi.org/10.1177/186810341203100202
- Siregar, W. Z. Br. (2005). Parliamentary Representation of Women in Indonesia: The Struggle for a Quota. Asian Journal of Women’s Studies, 11(3), 36–72. https://doi.org/10.1080/12259276.2005.11665993
- Warburton, E. (2016). Jokowi and the New Developmentalism. Bulletin of Indonesian Economic Studies, 52(3), 297–320. https://doi.org/10.1080/00074918.2016.1249262
- Winters, J. A. (2016). Electoral Dynamics in Indonesia: Money Politics, Patronage and Clientelism at the Grassroots. Bulletin of Indonesian Economic Studies, 52(3), 405–409. https://doi.org/10.1080/00074918.2016.1236653


---

<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>
